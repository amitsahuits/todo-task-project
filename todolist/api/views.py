from django.shortcuts import render
from rest_framework import authentication
# from requests import api
from app.models import taskmodel
from .serializers import taskSerializer

from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated

from rest_framework import viewsets

class taskModelViewSet(viewsets.ModelViewSet):
    queryset = taskmodel.objects.all()
    serializer_class = taskSerializer
    #we installed basic authentication here.
    authentication_classes = [BasicAuthentication]
    
    #allow any authenticated user
    permission_classes = [IsAuthenticated]
