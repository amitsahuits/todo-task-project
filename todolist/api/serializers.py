from rest_framework import serializers
from app.models import taskmodel

class taskSerializer(serializers.ModelSerializer):
    class Meta:
        model = taskmodel
        fields = ['id', 'user', 'task', 'date']