from django import forms
from .models import taskmodel

class TaskForm(forms.ModelForm):
    class Meta:
        model = taskmodel
        fields = ['task']