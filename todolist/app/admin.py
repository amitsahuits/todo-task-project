from django.contrib import admin

from .models import taskmodel

# Register your models here.
@admin.register(taskmodel)
class TaskModelAdmin(admin.ModelAdmin):
    list_display = ['id','user','task','date']