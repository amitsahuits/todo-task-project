from django.shortcuts import render,HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm, UsernameField
from django.contrib.auth import authenticate,login,logout
from django.contrib import messages

from app.models import taskmodel
from .forms import TaskForm
# Create your views here.

def user_login(request):
    #if user is authenticated then we did not let him see login page
    #if user is not logged in already only then this processes is going to work
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = AuthenticationForm(request=request, data=request.POST)
            if form.is_valid():
                uname = form.cleaned_data['username']
                upass = form.cleaned_data['password']
                
                #if there is any user with this password then we will user object, other wise none. 
                user = authenticate(username=uname,password=upass)
                if user is not None:
                    login(request, user)
                    messages.success(request,'Logged in Successfully !!')
                    return HttpResponseRedirect('/todo/')
        else:
            form = AuthenticationForm()
        return render(request, 'login.html', {'form':form})
    # if user is authenticated 
    else:
        return HttpResponseRedirect('/todo/')



def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')


def todo(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            messages.success(request,'task added successfully')
            task = form.cleaned_data['task']
            t = taskmodel(task=task, user=request.user)
            t.save()
            task = taskmodel.objects.filter(user__exact=request.user)
    else:
        task = taskmodel.objects.filter(user__exact=request.user)
        form = TaskForm()
    return render(request,'todo.html',{'form':form,'task':task})


def user_signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            messages.success(request,'Congratulation! You are now a member of our large community!')
            form.save()
            #adding user directly to the auther group so they can use permission which is allowed for this auther group
            HttpResponseRedirect('/login/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html',{'form':form})

